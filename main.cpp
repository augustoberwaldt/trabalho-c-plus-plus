#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include "Produto.h"
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <stdio.h>
#include "ClassCurl.h"



using namespace std;
void  menu()
{


    cout << endl;
    cout <<  "-----------------------"  << endl;
    cout << "|        Menu           |" << endl;
    cout <<  "-----------------------"  << endl;
    cout << "| 1 - Cadastrar Produto |" << endl;
    cout << "| 2 - Atualizar Produto |" << endl;
    cout << "| 3 - Listar  Produto   |" << endl;
    cout << "| 4 - Deletar Produto   |" << endl;
    cout << "| 5 - Exportar para Csv |" << endl;
    cout << "| 6 - Importar  Csv     |" << endl;
    cout << "| 7 - Sincronizar Web   |" << endl;
    cout << "| 8 - Sair              |" << endl;
    cout << " -----------------------"  << endl;
}

void  menuTitulo(string titulo){
    cout << endl;
    cout <<  "--------------------------------------"  << endl;
    cout << "|      " <<titulo<<"                   |" << endl;
    cout << " --------------------------------------"  << endl;
}


int main(int argc, char* argv[])
{

    vector<Produto> produtos;
    int opcao = 0;

    string folder = __FILE__;
    std::size_t pos = folder.find("main");
    string path =  folder.substr(0,pos);

    do {
        menu();
        cin >> opcao;

        system("clear");
        switch (opcao) {
            case 1: {
                menuTitulo("Cadastrar Produto");
                Produto produto;
                string titulo , descricao, codigo;
                int qtd;
                float preco;

                cout << "Informe o Codigo :"    << endl;
                cin >> codigo;

                cout << "Informe o Titulo :"    << endl;
                cin >> titulo;

                cout << "Informe o Descricao :" << endl;
                cin >> descricao;

                cout << "Informe o Estoque :"   << endl;
                cin >> qtd;

                cout << "Informe o Preco :"     << endl;
                cin >> preco;

                produto.setTitulo(descricao);
                produto.setCodigo(codigo);
                produto.setDescricao(descricao);
                produto.setEstoque(qtd);
                produto.setPreco(preco);
                produtos.push_back(produto);

                break;
            }
            case 2: {
                menuTitulo("Atualizar Produto");
                cout << "Informe o codigo do produto :" <<endl;
                string codigo;
                cin >> codigo;

                vector<Produto>::iterator it = find_if(produtos.begin(), produtos.end(),
                                                       [codigo](Produto obj) {
                                                           return obj.getCodigo() == codigo;
                                                       });

                cout << it->getCodigo() << " | " << it->getTitulo() << "\t\t | \t" << it->getEstoque() << endl;

                break;
            }

            case 3: {
                menuTitulo("Listar  Produto");
                for(int i = 0; i < produtos.size(); i++) {
                    cout << produtos[i].getCodigo() << " | " << produtos[i].getTitulo() << "\t\t | \t" << produtos[i].getEstoque() << endl;
                }
                cout << "------------------------------------------------";
                break;
            }
            case 4: {
                menuTitulo("Deletar  Produto");
                cout << "Informe o codigo do produto :" <<endl;
                string codigo;
                cin >> codigo;

                vector<Produto>::iterator it = find_if(produtos.begin(), produtos.end(),
                                                       [codigo](Produto obj) {
                                                           return obj.getCodigo() == codigo;
                                                       });
                produtos.erase(it);

                break;
            }
            case 5: {
                menuTitulo("Exportar para Csv");
                ofstream file;
                file.open(path + "produtosExport.csv", fstream::out);
                if (!file.is_open()) {
                    cerr << "Error Arquivo";
                    exit(1);
                }
                file << "CODIGO_PRODUTO;TITULO_PRODUTO;DESCRICAO_PRODUTO;ESTOQUE_PRODUTO;PRECO_PRODUTO";
                for (vector<Produto>::iterator it = produtos.begin(); it != produtos.end(); ++it) {
                    file << it->getCodigo()    << ";"
                         << it->getTitulo()    << ";"
                         << it->getDescricao() << ";"
                         << it->getEstoque()   <<  ";" << it->getPreco()    << ";" << endl ;
                }

                break;
            }
            case 6: {
                menuTitulo("Importar  Produto Csv");
                ifstream file (path + "importacaoProduto.csv");
                string value="";
                getline(file, value);
                vector< map<int, string> > fileConverte;
                while ( getline(file, value))
                {
                    stringstream strstr(value);
                    string word = "";

                    map<int, string> prod;
                    int count = 0;
                    while (getline(strstr,word, ';')) {
                        prod.insert(make_pair(count, word));
                        count++;
                    }
                    fileConverte.push_back(prod);
                }
                for(int i = 0; i < fileConverte.size(); i++) {
                    Produto prod_tmp;
                    map<int, string> tmp =  fileConverte[i];
                    prod_tmp.setCodigo(tmp[0]);
                    prod_tmp.setTitulo(tmp[1]);
                    prod_tmp.setDescricao(tmp[2]);
                    prod_tmp.setEstoque(boost::lexical_cast<int>(tmp[3]));
                    prod_tmp.setPreco(boost::lexical_cast<float>(tmp[4]));
                    produtos.push_back(prod_tmp);
                }

                break;
            }
            case 7: {
                menuTitulo("Sincronizar web");
                for(int i = 0; i < produtos.size(); i++) {
                    ClassCurl<Produto> objCurl(produtos[i]);
                    objCurl.setUri("http://localhost/trabalhoweb/Api/salvaProduto");
                    string param = "titulo=" + produtos[i].getTitulo() +"&preco=" + boost::lexical_cast<string>(produtos[i].getEstoque());
                    objCurl.sedPost(param);
                }
            }
        }
    } while (opcao != 8);

    return 0;
}


