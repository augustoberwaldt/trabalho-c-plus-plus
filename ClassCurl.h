//
// Created by developer on 21/06/16.
//

#ifndef TRABALHO_C_PLUS_PLUS_CLASSCURL_H
#define TRABALHO_C_PLUS_PLUS_CLASSCURL_H
#include <string>
#include <vector>
#include <curl/curl.h>
#include <string>
using namespace std;
template<typename T> class ClassCurl {

    private:
    char *uri;
    T dado;

    public:
        ClassCurl(T dado);
       void sedPost(string dados);
       void setUri(char* url);
       char * getUri();

};



template<typename T>  ClassCurl<T>::ClassCurl(T dado){
    this->dado = dado;
}

template<typename T>void ClassCurl<T>::setUri(char* url) {
    this->uri = url;
}

template<typename T>char *  ClassCurl<T>::getUri() {
    return this->uri;
}

template<typename T>void  ClassCurl<T>::sedPost(string dados)
{

    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "http://localhost/trabalhoweb/Api/salvaProduto");
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, dados.c_str());
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }

}
#endif //TRABALHO_C_PLUS_PLUS_CLASSCURL_H
