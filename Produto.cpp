/**
 *
 * @author Augusto Marlon
 *
 */

#include "Produto.h"
#include <boost/lexical_cast.hpp>
/**
 * Seta o Titulo
 *
 * @param const string
 */
void Produto::setTitulo(const string titulo)
{
    this->titulo = titulo;
}
/**
 * Seta o descricao
 *
 * @param const string
 */
void Produto::setDescricao(const string descricao)
{
    this->descricao = descricao;
}
/**
 * Seta o estoque
 *
 * @param const int
 */
void Produto::setEstoque(const int estoque)
{
    this->estoque = estoque;
}
/**
 * Seta o preco
 *
 * @param const float
 */
void Produto::setPreco(const float preco)
{
    this->preco = preco;
}

/**
 * Seta o codigo
 *
 * @param const string
 */
void Produto::setCodigo(const string codigo)
{
    this->codigo = codigo;
}

/**
 * pega o codigo
 *
 * @param const string
 */
string Produto::getCodigo()
{
    return this->codigo;
}

/**
 * pega o Titulo
 *
 * @return string
 */
string Produto::getTitulo()
{
    return this->titulo;
}

/**
 * pega a Descricao
 *
 * @return string
 */
string Produto::getDescricao()
{
    return this->descricao;
}


/**
 * pega o Estoque
 *
 * @return int
 */
int Produto::getEstoque()
{
    return this->estoque;
}

/**
 * pega o Estoque
 *
 * @return int
 */
float  Produto::getPreco()
{
    return this->preco;
}

char*  Produto::toStringPost()
{

    string estoque =boost::lexical_cast<string>(this->getEstoque());
    return "";
}


