
#ifndef UNTITLED2_PRODUTO_H
    #define UNTITLED2_PRODUTO_H

/**  includes **/
#include <iostream>
#include "Serializer.h"
/**  namespaces **/
using namespace std;


class Produto : Serializer {

    public:
        void setTitulo(const string titulo);
        void setDescricao(const string descricao);
        void setEstoque(const int estoque);
        void setPreco(const float preco);
        void setCodigo(const string codigo);
        string getCodigo();
        string getTitulo();
        string getDescricao();
        int    getEstoque();
        float  getPreco();
        char*  toStringPost();

private:
        string codigo;
        string titulo;
        string descricao;
        int    estoque;
        float  preco;

};
#endif